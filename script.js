// Tux Timer 2024 Arnaud Champollion, licence GNU/GPL 3.0

// Constituants de la page
const path = document.getElementById('myCircle');
const pathSecondes = document.getElementById('myCircleSecondes');
const start = document.getElementById('start');
const boutonCompteurToujoursVisible = document.getElementById('bouton-compteur-toujours-visible');
const horloge = document.getElementById('horloge');
const cercleSecondes = document.getElementById('cercle-secondes');
const poignee = document.getElementById('poignee');
const imgMinutes = document.getElementById('minutes');
const compteur = document.getElementById('compteur');
const compteurBis = document.getElementById('compteur-bis');
const compteurMinutesBis = document.getElementById('compteur-minutes-bis');
const compteurSecondesBis = document.getElementById('compteur-secondes-bis');
const compteurMinutes = document.getElementById('compteur-minutes');
const compteurSecondes = document.getElementById('compteur-secondes');
const boutonPlus = document.getElementById('bouton-plus');
const boutonMoins = document.getElementById('bouton-moins');
const boutonHoraire = document.getElementById('sens-horaire');
const boutonAntiHoraire = document.getElementById('sens-anti-horaire');
const boutonCouleur = document.getElementById('bouton-couleur');
const boutonAPropos = document.getElementById('bouton-a-propos');
const boutonOptions = document.getElementById('bouton-options');
const divSons = document.getElementById('sons');
const divAPropos = document.getElementById('div-a-propos');
const divOptions = document.getElementById('div-options');
const divAffichage = document.getElementById('zone-affichage');
const boutonSons = document.getElementById('bouton-sons');
const boutonSonA = document.getElementById('bouton-a');
const boutonSonB = document.getElementById('bouton-b');
const boutonSonC = document.getElementById('bouton-c');
const boutonSonD = document.getElementById('bouton-d');
const boutonSonE = document.getElementById('bouton-e');
const boutonSonF = document.getElementById('bouton-f');
const body = document.body;


// Bouton son, fichier son, image du bouton
listeSons = [
    [boutonSonA,"","no-sound.svg"],
    [boutonSonB,"metallophone.mp3","metallophone.svg"],
    [boutonSonC,"coq.mp3","coq.svg"],
    [boutonSonD,"rouge-gorge.mp3","rouge-gorge.svg"],
    [boutonSonE,"chat.mp3","chat.svg"],
    [boutonSonF,"applaudissements.mp3","applaudissements.svg"],
]

// Est-on dans Openboard ?
openboard = Boolean(window.widget || window.sankore);
console.log('Sommes-nous dans Openboard ? '+Boolean(window.widget || window.sankore));
console.log('Y-a-t-il une fonctionnalité tactile sur cet appareil ? '+("ontouchstart" in document.documentElement))
console.log('Y-a-t-il une fonctionnalité souris sur cet appareil ? '+("onclick" in document.documentElement))

// Son par défaut
notification = new Audio('sons/metallophone.mp3');

// Variables de départ
angle = 360;
angleSecondes = 360;
compteurMinutesBis.innerText=compteurMinutes.innerText='0';
compteurSecondesBis.innerText=compteurSecondes.innerText='00';
start.disabled=true;
actionContinueEnCours=false;
boutonCouleur.value="#0000ff";
pause=true;
sonOn=true;
let regex = /^[0-9]+$/;
frequence=200;
continuousActionInterval=null;
derniereMinute=false;
depart=0;
compteurToujoursVisible=false;
tempsRestant=0;
autostart=false;
inverse=false;

// Cercle des secondes
pathSecondes.setAttribute('d', "M 50, 50 m -50, 0 a 50,50 0 1,0 100,0 a 50,50 0 1,0 -100,0");


////////// On vérifie si des paramètres sont dans le stockage /////////

if (!openboard){ // Sauf dans Openboard

    if (localStorage.getItem('tuxtimer-sens')){
        sens(localStorage.getItem('tuxtimer-sens'))
    } else {
        sens('anti-horaire');
    }

    if (localStorage.getItem('tuxtimer-couleur')){
        changeCouleur(localStorage.getItem('tuxtimer-couleur'));
        boutonCouleur.value=localStorage.getItem('tuxtimer-couleur');
    }

    if (localStorage.getItem('tuxtimer-son')){
        let index=parseInt(localStorage.getItem('tuxtimer-son'));
        changeSon(listeSons[index][1],listeSons[index][2],index);
    }

    if (localStorage.getItem('tuxtimer-sens')){
        sens(localStorage.getItem('tuxtimer-sens'));
    }

    if (localStorage.getItem('tuxtimer-compteur-toujours-visible')){
        compteurToujoursVisible=localStorage.getItem('tuxtimer-compteur-toujours-visible')==='true';
        if (compteurToujoursVisible){compteurBis.classList.remove('hide');}      
    }
}

///////////////////////////////////////////////////////////////////////

// On vérifie si des paramètres sont dans l'URL
let url = window.location.search;
let urlParams = new URLSearchParams(url);

if (urlParams.get('primtuxmenu')==="true") {
    body.style.backgroundImage='url(images/primtux.png)';
}

if (urlParams.get('delai')) {frequenceDepart=parseInt(urlParams.get('delai'));}
else {frequenceDepart=200;}


testInverse=(urlParams.get('sens')==='horaire');
if (testInverse){
    sens('horaire');
}

if (urlParams.get('minutes')) {
    compteurMinutes.innerText=compteurMinutesBis.innerText=urlParams.get('minutes');
    changeTemps();
}

if (urlParams.get('autostart')==='true') {
    autostart=true;
}

if (urlParams.get('couleur')) {
    let texteCouleur = urlParams.get('couleur');
    let hexadecimal = texteVersHex(texteCouleur);
    let nouvelleCouleur=null;
    let regexHex = /^[0-9A-F]{6}$/i;
    if (hexadecimal){nouvelleCouleur=hexadecimal}
    else if (regexHex.test(texteCouleur)){nouvelleCouleur='#'+texteCouleur}
    if (nouvelleCouleur){
        changeCouleur(nouvelleCouleur);
        boutonCouleur.value=nouvelleCouleur;
    }
}


boutonCompteurToujoursVisible.checked=compteurToujoursVisible;

//////////////////////////////////////////////////////////////////////////

// Attribution des images et sons aux boutons
let tour = 0;
listeSons.forEach((element, index) => {
    let fichierImage = element[1];
    let fichierSon = element[2];
    element[0].style.backgroundImage = 'url(images/' + fichierSon + ')';
    element[0].addEventListener("click", (function(image, son, tour) {
        return function() {
            changeSon(image, son, tour);
        };
    })(fichierImage, fichierSon, tour));
    tour += 1;
});

//////////////////////////////////////////////////////////////////////////
if (autostart){
    commencer('auto');
}

//////////////////////////////////////////////////////////////////////////

// Choix du sens horaire ou anti-horaire
function sens(valeur){
    if (valeur==="horaire"){
        imgMinutes.src='images/minutes-inverse.svg';
        horloge.style.transform='scaleX(1)';
        cercleSecondes.style.transform='translateX(-50%) translateY(-50%) scaleX(1)';
        boutonHoraire.classList.add('select');
        boutonAntiHoraire.classList.remove('select');
        inverse=true;
    } else {
        imgMinutes.src='images/minutes.svg';
        horloge.style.transform='scaleX(-1)';
        cercleSecondes.style.transform='translateX(-50%) translateY(-50%) scaleX(-1)';
        boutonHoraire.classList.remove('select');
        boutonAntiHoraire.classList.add('select');
        inverse=false; 
    }
    changeTemps();
    if (!openboard){localStorage.setItem('tuxtimer-sens',valeur);}
}

// Quand les temps est écoulé
function arreter(){
    tempsRestant=0;
    pause=true;
    compteurSecondesBis.innerText=compteurMinutesBis.innerText=compteurMinutes.innerText=compteurSecondes.innerText='00';
    angle=0;
    angleSecondes=0;
    path.setAttribute('d', describeArc(50, 50, 50, 0, angle));
    pathSecondes.setAttribute('d', describeArc(50, 50, 50, 0, 0));
    start.disabled=true;
    start.style.backgroundPositionY='bottom';
    body.style.backgroundColor='red';
    if (sonOn){notification.play();}
}

//
function visibiliteCompteur(valeur) {
    compteurToujoursVisible=valeur;
    if (compteurToujoursVisible){compteurBis.classList.remove('hide');}
    else if (tempsRestant>60000){compteurBis.classList.add('hide');}
    if (!openboard)[localStorage.setItem('tuxtimer-compteur-toujours-visible',compteurToujoursVisible)]
}

// Mise à jour du son de notification
function changeSon(fichierSon,fichierImage,index){
    if (fichierSon != ''){
        notification.src='sons/'+fichierSon;
        sonOn=true;
    } else {
        sonOn=false;
    }
    boutonSons.style.backgroundImage='url(images/'+fichierImage+')';
    divSons.classList.add('hide');
    if (!openboard){localStorage.setItem('tuxtimer-son',index);}

}

// Vérifier si la valeur entrée dans le compteur est correcte avant de l'appliquer
function verifieCompteur(){
    let nombre=parseInt(compteurMinutes.innerText);
    if (nombre>0){
        if (nombre>60){compteurMinutesBis.innerText=compteurMinutes.innerText=60;compteurSecondesBis.innerText=compteurSecondes.innerText='00';}
        start.disabled=false;
    } else {
        start.disabled=true;
    }
    if (nombre===0){
        compteurBis.classList.remove('hide');
    } else {
        if (!compteurToujoursVisible){compteurBis.classList.add('hide');}
    }
}

// Ouvrir les panneaux à propos ou options
function aPropos(){
    divAPropos.classList.toggle('hide');
}

function options(){
    divOptions.classList.toggle('hide');
}

// Réduction progressive du disque
function reduceCircle() {
    if (!pause){
        maintenant = new Date().getTime();
        tempsEcoule = maintenant - depart;
        if (tempsRestant>=0){

            let secondesRestantes = tempsRestant/1000;
            
            let secondesRestantesDate = Math.round((tempsDepart - tempsEcoule)/1000);
        
            if (secondesRestantesDate != secondesRestantes){
                secondesRestantes=secondesRestantesDate;
                tempsRestant=secondesRestantes*1000;
            }

            angle = secondesRestantes/10;
            let minutes=Math.floor(secondesRestantes/60);
            let secondes=Math.floor(secondesRestantes%60);

            if (minutes===0) {
                if(!derniereMinute) {
                    derniereMinute=true;
                    compteurBis.classList.remove('hide');
                }
            } else if (derniereMinute){
                derniereMinute=false;
                if (!compteurToujoursVisible){compteurBis.classList.add('hide');}
            }
            if (secondesRestantes<60){                
                angleSecondes=secondesRestantes*6;
                pathSecondes.setAttribute('d', describeArc(50, 50, 50, 0, angleSecondes));
            }

            path.setAttribute('d', describeArc(50, 50, 50, 0, angle));

            compteurMinutesBis.innerText=compteurMinutes.innerText=minutes;
            if (secondes<10){compteurSecondesBis.innerText=compteurSecondes.innerText='0'+secondes;}
            else {compteurSecondesBis.innerText=compteurSecondes.innerText=secondes;}

            if (tempsRestant >= 1000) {
                tempsRestant-=1000;  
                setTimeout(reduceCircle, 1000);
            }
            else {
                pause=true;
                start.disabled=true;
                start.style.backgroundPositionY='bottom';
                body.style.backgroundColor='red';
                if (sonOn){notification.play();}
            }
        }
        deplacePoignee();            
    }
}








function boutonEnfonce(valeur){
    if (!actionContinueEnCours){
        frequence=frequenceDepart;
        plusMoinsTemps(valeur);
        actionContinueEnCours=true;
        continuousActionInterval = setInterval(function() {
            plusMoinsTemps(valeur);
        }, frequence);
    }
}

function boutonRelache(){
    actionContinueEnCours=false;
    if (continuousActionInterval){clearInterval(continuousActionInterval);}
}


function plusMoinsTemps(valeur){
    
    if (actionContinueEnCours && frequence >= 50){
        frequence -= 10;
        clearInterval(continuousActionInterval);
        continuousActionInterval = setInterval(function() {
            plusMoinsTemps(valeur);
        }, frequence);
    }
    
    if (compteurMinutes.innerText===''){compteurMinutesBis.innerText=compteurMinutes.innerText='0';}    
    let actuel = parseInt(compteur.innerText);
    let nouveau = actuel + valeur;
    compteurSecondesBis.innerText=compteurSecondes.innerText='00';
    if ( nouveau <= 0 ){
        boutonRelache()
        compteurMinutesBis.innerText=compteurMinutes.innerText='00';
    } else if ( nouveau >= 60 ){
        boutonRelache()
        compteurMinutesBis.innerText=compteurMinutes.innerText='60';
    } else {
        compteurMinutesBis.innerText=compteurMinutes.innerText=nouveau;        
    }
    deplacePoignee();
    changeTemps();
    
}

function deplacePoignee(){
    let hypothenuse=35;
    let deplacementHorizontal;
    let deplacementVertical;

    deplacementHorizontal = Math.sin(angle*Math.PI/180)*hypothenuse;
    deplacementVertical = hypothenuse - Math.cos(angle*Math.PI/180)*hypothenuse;

    if (inverse){deplacementHorizontal=-deplacementHorizontal;}

    poignee.style.left=(50 - deplacementHorizontal) + '%';
    poignee.style.top=(15 + deplacementVertical) + '%';

}

function changeTemps(){
    pathSecondes.setAttribute('d', "M 50, 50 m -50, 0 a 50,50 0 1,0 100,0 a 50,50 0 1,0 -100,0");
    tempsRestant=60000*parseInt(compteurMinutes.innerText);
    maintenant = new Date().getTime();
    tempsDepart=tempsRestant;
    depart=maintenant;
    body.style.backgroundColor=null;
    angle=tempsRestant/10000;
    
    if (tempsRestant>0){
        start.disabled=false;
    }

    if (angle!=360){
    path.setAttribute('d', describeArc(50, 50, 50, 0, angle));

    } else {
        path.setAttribute('d', "M 50, 50 m -50, 0 a 50,50 0 1,0 100,0 a 50,50 0 1,0 -100,0");
    }

    if (tempsRestant===0){
        compteurBis.classList.remove('hide');

    } else {
        if (!compteurToujoursVisible){compteurBis.classList.add('hide');}
    }
    deplacePoignee();
}

function changeCouleur(couleur){
    path.setAttribute('fill',couleur);
    if (!openboard){localStorage.setItem('tuxtimer-couleur',couleur);}
}

function sons(){
    divSons.classList.toggle('hide');
}

function choixSon(son,bouton){

}

function clic(event){
    if (!estEnfantDe(event.target,divAPropos) && event.target!=divAPropos && event.target!=boutonAPropos && event.target){
        divAPropos.classList.add('hide');
    }
    if (!estEnfantDe(event.target,divOptions) && event.target!=divOptions && event.target!=boutonOptions && event.target){
        divOptions.classList.add('hide');
    }
}

function estEnfantDe(element,parent) {
    // Parcours les parents de l'élément donné
    while (element.parentNode) {
        element = element.parentNode;
        // Si un des parents est l'élément menu, retourne vrai
        if (element === parent) {
            return true;
        }
    }
    // Si aucun parent n'est l'élément menu, retourne faux
    return false;
}

// Appui sur la touche entrée ou bouton play / pause
function commencer(mode){
    compteur.blur();
    maintenant = new Date().getTime();
    tempsDepart=tempsRestant;

    if (pause){
        pause=false;
        body.style.backgroundColor=null;
        start.style.backgroundPositionY='top';     

        if(maintenant - depart > 300 || mode==='auto'){
            depart = maintenant;
            reduceCircle();
        } 

    } else {
        pause=true;
        start.style.backgroundPositionY='bottom';
    }

    depart = maintenant; 

}

function polarToCartesian(centerX, centerY, radius, angleInDegrees) {
    var angleInRadians = (angleInDegrees - 90) * Math.PI / 180.0;

    return {
        x: centerX + (radius * Math.cos(angleInRadians)),
        y: centerY + (radius * Math.sin(angleInRadians))
    };
}

function describeArc(x, y, radius, startAngle, endAngle) {

    var start = polarToCartesian(x, y, radius, endAngle);
    var end = polarToCartesian(x, y, radius, startAngle);

    var largeArcFlag = endAngle - startAngle <= 180 ? "0" : "1";

    var d = [
        "M", start.x, start.y,
        "A", radius, radius, 0, largeArcFlag, 0, end.x, end.y,
        "L", x, y,
        "Z"
    ].join(" ");

    return d;
}



document.addEventListener("click",clic);
document.addEventListener("touchstart",clic);


compteurMinutes.addEventListener("input", function(event) {
  let cursorPosition = getCaretPosition(this); // Enregistre la position du curseur
  compteurMinutes.innerText=compteurMinutesBis.innerText=(compteurMinutes.innerText).replace(/\D/g, '');
  setCaretPosition(this, cursorPosition); // Restaure la position du curseur
  verifieCompteur();
  changeTemps();
  });
  

compteurMinutes.addEventListener("focus", function(event) {
    if (!pause){pause=true;start.style.backgroundPositionY='bottom';}
    compteurSecondesBis.innerText=compteurSecondes.innerText='00';
    let selection = window.getSelection();
    let range = document.createRange();
    range.selectNodeContents(compteurMinutes);
    selection.removeAllRanges();
    selection.addRange(range);
    if (compteur.innerText==='0'){compteurMinutes.innerText='';}
    });
    

movePoigneeEncours=false;
    
function clicPoignee(event){
    event.preventDefault();
    movePoigneeEncours=true;
    console.log('---- clic poignée ----');
}

function relachePoignee(){
    movePoigneeEncours=false;
    console.log('---- relâchement poignée ----');
}

function movePoignee(){
    // Récupérer les dimensions de la div
    var rect = divAffichage.getBoundingClientRect();
    var divWidth = rect.width;
    var divHeight = rect.height;

    // Récupérer la position de la souris par rapport à la div
    if (event.touches && event.touches.length > 0) {
        // pour le tactile
        mouseX = event.touches[0].clientX - rect.left;
        mouseY = event.touches[0].clientY - rect.top;
        console.log("Déplacement tactile " + mouseX+" "+mouseY);
    } else {
        // sinon
        mouseX = event.clientX - rect.left;
        mouseY = event.clientY - rect.top;
        console.log("Déplacement souris " + mouseX+" "+mouseY);

    }

    // Calculer la position en pourcentage
    var mouseXPercent = (mouseX / divWidth) * 100;
    var mouseYPercent = (mouseY / divHeight) * 100;
    
    let deplacementHorizontal;
    
    if (inverse){deplacementHorizontal = mouseXPercent - 50;}
    else {deplacementHorizontal = 50 - mouseXPercent;}
    
    let deplacementVertical = 50 - mouseYPercent;

    let nouvelAngleRadian = Math.atan(deplacementHorizontal/deplacementVertical);
    let nouvelAngleDegres = nouvelAngleRadian*180/Math.PI;
    
    if (deplacementHorizontal>=0 && deplacementVertical>=0) {
        angle = nouvelAngleDegres;
    } else if (deplacementVertical<0) {
        angle = 180 + nouvelAngleDegres;
    } else {
        angle = 360 + nouvelAngleDegres;
    }
    
    if (movePoigneeEncours){
        compteurMinutes.innerText=compteurMinutesBis.innerText=Math.round(angle/6);
        changeTemps();
    }
}

    
poignee.addEventListener("mousedown", clicPoignee);
poignee.addEventListener("touchstart", clicPoignee);

divAffichage.addEventListener("mousemove", movePoignee);
divAffichage.addEventListener("touchmove", movePoignee);

document.addEventListener("mouseup", relachePoignee);
document.addEventListener("touchend", relachePoignee);


boutonPlus.addEventListener("touchstart", function(event) {
    //event.preventDefault();
    boutonEnfonce(1);
});

boutonMoins.addEventListener("touchstart", function(event) {
    //event.preventDefault();
    boutonEnfonce(-1);
});

boutonPlus.addEventListener("touchend", function(event) {
    boutonRelache();
  });

boutonMoins.addEventListener("touchend", function(event) {
    boutonRelache();
});

boutonPlus.addEventListener("touchcancel", function(event) {
    boutonRelache();
});

boutonMoins.addEventListener("touchcancel", function(event) {
    boutonRelache();
});

document.addEventListener("keypress", function(event) {
    // Vérifier si la touche appuyée est la touche "Entrée"
    if (event.keyCode === 13) {
      compteurMinutes.blur(); 
      if (parseInt(compteur.innerText)>0){commencer();}
    }

    // Récupérer le code de la touche
    var codeTouche = event.keyCode || event.which;
    
    // Convertir le code de la touche en caractère
    var caractereTouche = String.fromCharCode(codeTouche);

    // Vérifier si la touche est un chiffre
    if (compteurMinutes!=document.activeElement && estChiffre(caractereTouche)) {
        if (compteurMinutes.innerText==='0'){compteurMinutesBis.innerText=compteurMinutes.innerText='';}
        compteurSecondesBis.innerText=compteurSecondes.innerText='00';
        compteurMinutesBis.innerText=compteurMinutes.innerText+=caractereTouche;        
        verifieCompteur();
    }
});


  function estChiffre(touche) {
    return /^[0-9]$/.test(touche);
  }


  // Fonction pour obtenir la position du curseur
function getCaretPosition(element) {
    const selection = window.getSelection();
    return selection.focusOffset;
}

// Fonction pour définir la position du curseur
function setCaretPosition(element, position) {
    const selection = window.getSelection();
    const range = document.createRange();
    range.setStart(element.firstChild, Math.min(position, element.innerText.length));
    range.collapse(true);
    selection.removeAllRanges();
    selection.addRange(range);
}

function texteVersHex(texteCouleur) {
    // Tableau de correspondance des couleurs nommées
    var couleurs = {
        "aliceblue": "#F0F8FF",
        "antiquewhite": "#FAEBD7",
        "aqua": "#00FFFF",
        "aquamarine": "#7FFFD4",
        "azure": "#F0FFFF",
        "beige": "#F5F5DC",
        "bisque": "#FFE4C4",
        "black": "#000000",
        "blanchedalmond": "#FFEBCD",
        "blue": "#0000FF",
        "blueviolet": "#8A2BE2",
        "brown": "#A52A2A",
        "burlywood": "#DEB887",
        "cadetblue": "#5F9EA0",
        "chartreuse": "#7FFF00",
        "chocolate": "#D2691E",
        "coral": "#FF7F50",
        "cornflowerblue": "#6495ED",
        "cornsilk": "#FFF8DC",
        "crimson": "#DC143C",
        "cyan": "#00FFFF",
        "darkblue": "#00008B",
        "darkcyan": "#008B8B",
        "darkgoldenrod": "#B8860B",
        "darkgray": "#A9A9A9",
        "darkgreen": "#006400",
        "darkgrey": "#A9A9A9",
        "darkkhaki": "#BDB76B",
        "darkmagenta": "#8B008B",
        "darkolivegreen": "#556B2F",
        "darkorange": "#FF8C00",
        "darkorchid": "#9932CC",
        "darkred": "#8B0000",
        "darksalmon": "#E9967A",
        "darkseagreen": "#8FBC8F",
        "darkslateblue": "#483D8B",
        "darkslategray": "#2F4F4F",
        "darkslategrey": "#2F4F4F",
        "darkturquoise": "#00CED1",
        "darkviolet": "#9400D3",
        "deeppink": "#FF1493",
        "deepskyblue": "#00BFFF",
        "dimgray": "#696969",
        "dimgrey": "#696969",
        "dodgerblue": "#1E90FF",
        "firebrick": "#B22222",
        "floralwhite": "#FFFAF0",
        "forestgreen": "#228B22",
        "fuchsia": "#FF00FF",
        "gainsboro": "#DCDCDC",
        "ghostwhite": "#F8F8FF",
        "gold": "#FFD700",
        "goldenrod": "#DAA520",
        "gray": "#808080",
        "green": "#008000",
        "greenyellow": "#ADFF2F",
        "grey": "#808080",
        "honeydew": "#F0FFF0",
        "hotpink": "#FF69B4",
        "indianred": "#CD5C5C",
        "indigo": "#4B0082",
        "ivory": "#FFFFF0",
        "khaki": "#F0E68C",
        "lavender": "#E6E6FA",
        "lavenderblush": "#FFF0F5",
        "lawngreen": "#7CFC00",
        "lemonchiffon": "#FFFACD",
        "lightblue": "#ADD8E6",
        "lightcoral": "#F08080",
        "lightcyan": "#E0FFFF",
        "lightgoldenrodyellow": "#FAFAD2",
        "lightgray": "#D3D3D3",
        "lightgreen": "#90EE90",
        "lightgrey": "#D3D3D3",
        "lightpink": "#FFB6C1",
        "lightsalmon": "#FFA07A",
        "lightseagreen": "#20B2AA",
        "lightskyblue": "#87CEFA",
        "lightslategray": "#778899",
        "lightslategrey": "#778899",
        "lightsteelblue": "#B0C4DE",
        "lightyellow": "#FFFFE0",
        "lime": "#00FF00",
        "limegreen": "#32CD32",
        "linen": "#FAF0E6",
        "magenta": "#FF00FF",
        "maroon": "#800000",
        "mediumaquamarine": "#66CDAA",
        "mediumblue": "#0000CD",
        "mediumorchid": "#BA55D3",
        "mediumpurple": "#9370DB",
        "mediumseagreen": "#3CB371",
        "mediumslateblue": "#7B68EE",
        "mediumspringgreen": "#00FA9A",
        "mediumturquoise": "#48D1CC",
        "mediumvioletred": "#C71585",
        "midnightblue": "#191970",
        "mintcream": "#F5FFFA",
        "mistyrose": "#FFE4E1",
        "moccasin": "#FFE4B5",
        "navajowhite": "#FFDEAD",
        "navy": "#000080",
        "oldlace": "#FDF5E6",
        "olive": "#808000",
        "olivedrab": "#6B8E23",
        "orange": "#FFA500",
        "orangered": "#FF4500",
        "orchid": "#DA70D6",
        "palegoldenrod": "#EEE8AA",
        "palegreen": "#98FB98",
        "paleturquoise": "#AFEEEE",
        "palevioletred": "#DB7093",
        "papayawhip": "#FFEFD5",
        "peachpuff": "#FFDAB9",
        "peru": "#CD853F",
        "pink": "#FFC0CB",
        "plum": "#DDA0DD",
        "powderblue": "#B0E0E6",
        "purple": "#800080",
        "rebeccapurple": "#663399",
        "red": "#FF0000",
        "rosybrown": "#BC8F8F",
        "royalblue": "#4169E1",
        "saddlebrown": "#8B4513",
        "salmon": "#FA8072",
        "sandybrown": "#F4A460",
        "seagreen": "#2E8B57",
        "seashell": "#FFF5EE",
        "sienna": "#A0522D",
        "silver": "#C0C0C0",
        "skyblue": "#87CEEB",
        "slateblue": "#6A5ACD",
        "slategray": "#708090",
        "slategrey": "#708090",
        "snow": "#FFFAFA",
        "springgreen": "#00FF7F",
        "steelblue": "#4682B4",
        "tan": "#D2B48C",
        "teal": "#008080",
        "thistle": "#D8BFD8",
        "tomato": "#FF6347",
        "turquoise": "#40E0D0",
        "violet": "#EE82EE",
        "wheat": "#F5DEB3",
        "white": "#FFFFFF",
        "whitesmoke": "#F5F5F5",
        "yellow": "#FFFF00",
        "yellowgreen": "#9ACD32"
    };

    // Vérifier si la couleur est dans le tableau
    if (texteCouleur.toLowerCase() in couleurs) {
        return couleurs[texteCouleur.toLowerCase()];
    } else {
        return false;
    }
}
 

