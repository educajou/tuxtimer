# Tux Timer

## Présentation

Tuxtimer est un compte à rebours graphique de 0 à 60 minutes.
Il permet à un élève ou un groupe d'élèves d'avoir une représentation du temps restant sans savoir lire l'heure sur un cadran.
Lorsque la souris quitte la zone de l'horloge, l'interface est volontairement épurée pour faciliter la concentration.

![](https://forge.apps.education.fr/educajou/tuxtimer/-/raw/main/screenshot.jpg?ref_type=heads)

## Fonctionnalités

- Choix de la couleur
- Choix du son de notification à la fin du temps écoulé
- Choix du sens de rotation
- Choix de la visibilité du compteur digital

## Utilisation

Il suffit de spécifier le nombre de minutes au clavier ou via les boutons - / + puis de cliquer sur le bouton "play".

## Prérequis

Tux Timer est une application en HTML / CSS / Javascript qui ne nécessite pas de base de données ni de langages côté serveur. L'application s'éxécute entièrement en local et ne stocke aucune information en ligne.
Elle peut fonctionner localement.

## Openboard

Une [application pour Openboard](https://forge.apps.education.fr/educajou/tuxtimer/-/blob/main/Tuxtimer.wgt.zip?ref_type=heads) est proposée. Elle fonctionne à partir de Openboard 1.7.
Pour savoir comment l'installer, une procédure est décrite [sur OpenÉdu](https://www.openedu.fr/openboard/installer-une-application-dans-openboard/).


## Démonstration

Une instance en fonctionnement est utilisable à l'adresse suivante :

https://educajou.forge.apps.education.fr/tuxtimer/

## Licence et crédits

Les graphismes utilisés pour les graduations et les chiffres proviennent de [l'horloge interactive de Thierry Munoz](https://thierrym.forge.apps.education.fr/horloge-ecole/), sous licence libre GPL 3.0.

Les images des boutons proviennent de https://www.svgrepo.com/ et sont sous licence "Public Domain".

Les notifications sonores, de Joseph Sardin, sont sous licence Creative Commons 0 et ont été récupérées depuis https://lasonotheque.org/.

La police de caractères utilisée pour l'affichage du compteur est Teleindicadore, sous Public Licence.

Tuxtimer est une application libre et gratuite créée par Arnaud Champollion, sous licence GNU/GPL 3.0.

